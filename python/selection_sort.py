#!/bin/python3

import numpy as np
import time

def swap(arr, i, j):
    tmp = arr[i]
    arr[i] = arr[j]
    arr[j] = tmp

def selection_sort(arr):   
    for i in range(0, len(arr) - 1):
        k = i
        
        for j in range(k + 1, len(arr)):
            if arr[k] > arr[j]:
                k = j
                
        swap(arr, k, i)

arr = np.random.randint(1, 100, 20)

print("Before sorting :", arr)

start = time.time()
selection_sort(arr)
end = time.time()

print("After sorting  :", arr)
print("Elapsed Time   :", (1000 * (end - start)), "sec")
