#!/bin/python3

import numpy as np
import time

def swap(arr, i, j):
    tmp = arr[i]
    arr[i] = arr[j]
    arr[j] = tmp

def insertion_sort(arr):   
    for i in range(1, len(arr)):
        # [i, 0)
        for j in range(i, 0, -1):
            if arr[j-1] > arr[j]:
                swap(arr, j-1, j)
                


arr = np.random.randint(1, 100, 20)

print("Before sorting :", arr)

start = time.time()
insertion_sort(arr)
end = time.time()

print("After sorting  :", arr)
print("Elapsed Time   :", (1000 * (end - start)), "sec")
