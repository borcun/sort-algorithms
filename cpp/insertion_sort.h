#ifndef INSERTION_SORT_H
#define INSERTION_SORT_H

#include "sort_alg.h"

class InsertionSort : public SortAlgorithm {
 public:
  InsertionSort( void );
  virtual ~InsertionSort();
  void perform( int *arr, const unsigned int len );
};

#endif
