#ifndef SORT_ALGORITHM_H
#define SORT_ALGORITHM_H

#include <iostream>

/** @class SortAlgorithm */
class SortAlgorithm
{
public:
  /** 
   * @brief default constructor 
   */
  SortAlgorithm( void );

  /** 
   * @brief destructor 
   */
  virtual ~SortAlgorithm();

  /** 
   * @brief virtual function that performs sorting operation 
   * @param [in|out] arr - array that contains numbers
   * @param [in] len - length of array
   * @return -
   */
  virtual void perform( int *arr, const unsigned int len ) = 0;

  /** 
   * @brief function that prints array 
   * @param [in|out] arr - array that contains numbers
   * @param [in] len - length of array
   * @return -
   */
  virtual void print( const int *arr, const unsigned int len );

  /**
   * @brief function that swaps two parameters
   * @param [in|out] num1 - first number 
   * @param [in|out] num2 - second number
   * @return -
   */
  void swap( int *num1, int *num2 );
  
};

#endif
