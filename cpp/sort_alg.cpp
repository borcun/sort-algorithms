#include "sort_alg.h"

SortAlgorithm::SortAlgorithm( void ) {

}

SortAlgorithm::~SortAlgorithm() {

}

void SortAlgorithm::print( const int *arr, const unsigned int len ) {
  if (NULL == arr) {
    return;
  }

  for (unsigned int i=0; i < len; ++i) {
    std::cout << arr[i] << " ";
  }

  std::cout << std::endl;

  return;
}

void SortAlgorithm::swap( int *num1, int *num2 ) {
  const int tmp = *num1;

  *num1 = *num2;
  *num2 = tmp;

  return;
}
