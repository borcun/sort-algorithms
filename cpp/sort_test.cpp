#include "sort_test.h"

SortTest::SortTest( void ) : m_count( 0 ), m_tested( false ) {

}

SortTest::~SortTest() {
  for (int i = 0; i < m_count; ++i) {
    delete m_sort_algs[i];
  }
}

bool SortTest::add( SortAlgorithm *sort_alg ) {
  if (NULL == sort_alg) {
    return false;
  }

  if (MAX_SORT_TEST_COUNT <= m_count) {
    return false;
  }

  m_sort_algs[ m_count ] = sort_alg;
  ++m_count;

  return true;
}

void SortTest::test( void ) {
  int arr[ MAX_SORT_ALGORITHM ][ MAX_ELEMENT_COUNT ];
  auto start = std::chrono::high_resolution_clock::now();
  auto end = std::chrono::high_resolution_clock::now();
  std::chrono::duration<double> diff = end - start;
    
  srand( time( NULL ) );

  for (int i = 0; i <  MAX_SORT_TEST_COUNT; ++i) {
    // fill first array with random numbers
    for (int k = 0; k < MAX_ELEMENT_COUNT; ++k) {
      arr[0][k] = ( rand() % MAX_ELEMENT_COUNT ) - ( MAX_ELEMENT_COUNT / 2 );
    }

    // copy first array to other array(s)
    for (int k = 1; k < m_count; ++k) {
      memcpy( arr[k], arr[0], sizeof( int ) * MAX_ELEMENT_COUNT );
    }

    for (int j = 0; j < m_count; ++j) {
      start = std::chrono::high_resolution_clock::now();
      m_sort_algs[j]->perform( arr[j], MAX_ELEMENT_COUNT );
      end = std::chrono::high_resolution_clock::now();

      diff = end - start;
      m_results[j][i] = diff.count();
    }
  }

  m_tested = true;

  return;
}

const double *SortTest::get( const int test_num ) const {
  if (false == m_tested) {
    return NULL;
  }

  if (test_num > m_count) {
    return NULL;
  }

  return m_results[test_num - 1];
}

