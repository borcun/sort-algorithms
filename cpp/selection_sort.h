#ifndef SELECTION_SORT_H
#define SELECTION_SORT_H

#include "sort_alg.h"

class SelectionSort : public SortAlgorithm {
 public:
  SelectionSort( void );
  virtual ~SelectionSort();
  void perform( int *arr, const unsigned int len );
};

#endif
