#include "selection_sort.h"

SelectionSort::SelectionSort( void ) {

}

SelectionSort::~SelectionSort() {

}

void SelectionSort::perform( int *arr, const unsigned int len ) {
  if (NULL == arr || 1 >= len) {
    std::cerr << "invalid parameters to sort array" << std::endl;

    return;
  }

  // do not perform sort for last element in array
  for (unsigned int i=0; i < len - 1; ++i) {
    unsigned int k = i;

    // find and hold the smallest number in remain of array and its index
    for (unsigned int j=i+1; j < len; ++j) {
      if (arr[j] < arr[k]) {
    	  k = j;
      }
    }

    // we expect that tmp is changed until here to swap
    if (k != i) {
      swap( &(arr[i]), &(arr[k]) );
    }
  }
  
  return;
}
