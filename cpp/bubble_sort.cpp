#include "bubble_sort.h"

BubbleSort::BubbleSort( void ) {

}

BubbleSort::~BubbleSort() {

}

void BubbleSort::perform( int *arr, const unsigned int len ) {
  if (NULL == arr || 1 >= len) {
    std::cerr << "invalid parameters to sort array" << std::endl;

    return;
  }

  for (unsigned int i=1; i < len; ++i) {
    for (unsigned int j=0; j < len - 1; ++j) {
      if (arr[j] > arr[j+1]) {
    	  swap( &(arr[j]), &(arr[j+1]) );
      }
    }
  }
  
  return;
}
