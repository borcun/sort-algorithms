#ifndef BUBBLE_SORT_H
#define BUBBLE_SORT_H

#include "sort_alg.h"

class BubbleSort : public SortAlgorithm {
 public:
  BubbleSort( void );
  virtual ~BubbleSort();
  void perform( int *arr, const unsigned int len );
};

#endif
