#ifndef SORT_TEST_H
#define SORT_TEST_H

#include "bubble_sort.h"
#include "selection_sort.h"
#include "insertion_sort.h"
#include <chrono>
#include <cstring>
#include <cstdlib>
#include <ctime>

//! maximum sort algorithm
#define MAX_SORT_ALGORITHM  (10)

//! maximum test count
#define MAX_SORT_TEST_COUNT (10)

//! sort element count
#define MAX_ELEMENT_COUNT   (10000)

class SortTest {
 public:
  /**
   * @brief default constructor 
   */
  SortTest( void );

  /**
   * @brief destructor 
   */
  virtual ~SortTest();

  /** 
   * @brief function that adds a sort algorithm into the alg. list 
   * @param [in] sort_alg - sort algorithm reference
   * @return true, if alg. is added. Otherwise, return false.
   */
  bool add( SortAlgorithm *sort_alg );

  /**
   * @brief function that performs sort algorithm(s)
   * @return -
   */
  void test( void );

  /**
   * @brief function that gets results of sort algorithm indicated by index 
   * @param [in] test_num - number of sort algorithm
   * @return algorithm result if algorithm was performed. Otherwise, return NULL.
   */
  const double *get( const int test_num ) const;

 private:
  //! sort algorithm list
  SortAlgorithm *m_sort_algs[ MAX_SORT_ALGORITHM ];

  //! sort algoritm result list
  double m_results[ MAX_SORT_ALGORITHM ][ MAX_SORT_TEST_COUNT ];

  //! sort algorithm count
  int m_count;

  //! test state flag
  bool m_tested;
  
};

#endif
