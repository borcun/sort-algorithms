#include "insertion_sort.h"

InsertionSort::InsertionSort( void ) {

}

InsertionSort::~InsertionSort() {

}

void InsertionSort::perform( int *arr, const unsigned int len ) {
  if (NULL == arr || 1 >= len) {
    std::cerr << "invalid parameters to sort array" << std::endl;

    return;
  }

  for (unsigned int i = 1; i < len; ++i) {
    for (unsigned int j = i; 0 != j; --j) {
      if (arr[j-1] > arr[j]) {
    	  swap( &(arr[j-1]), &(arr[j]) );
      }
    }
  }
  
  return;
}
