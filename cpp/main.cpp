#include "sort_test.h"
#include <cstdio>

int main(int argc, char **argv)
{
  SortTest sort;
  const double *result;

  if (false == sort.add( new InsertionSort() )) {
    std::cerr << "Insertion sort could not added" << std::endl;
  }

  if (false == sort.add( new SelectionSort() )) {
    std::cerr << "Selection sort could not added" << std::endl;
  }

  if (false == sort.add( new BubbleSort() )) {
    std::cerr << "Bubble sort could not added" << std::endl;
  }

  sort.test();

  for (int i = 1; i <= 3; ++i) {
    result = sort.get(i);

    std::cout << i << ". test: ";

    for (int j = 0; j < MAX_SORT_TEST_COUNT; ++j) {
      printf( "%.6f, ", result[j] );
    }

    std::cout << std::endl;
  }
  
  return 0;
}
